# Jellyfin Playlist Mixer

Given a playlist on a Jellyfin instance, this will create a new playlist that plays episodes from a random series order, BUT plays the episodes for each given series in order.  

Think of it like automatically creating a TV channel that has a random variety of shows but I'm always watching the episodes of each show in order